#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Script for updating crepo
"""
# -----------------------------------------------------------------------
# Import Modules
# -----------------------------------------------------------------------
import os
import configparser
import hashlib
import base64
import Crypto.Random
from Crypto.Cipher import AES
import git

# -----------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------
DEFAULT_GIT_BASE_URL = "gitlab.com"
DEFAULT_CREPO_PATH = "~/.tools/crepo/"
DEFAULT_REPO_PATH = "~/git/clusters"
DEFAULT_GIT_GROUP = "repos/"
DEFAULT_CLUSTER_LIST = "~/git/.clusters.list"
DEFAULT_ARGO_PATH = "~/git/argocd-cluster-config/"
SALT_SIZE = 16
NUMBER_OF_ITERATIONS = 20
AES_MULTIPLE = 16

# -----------------------------------------------------------------------
# Functions
# -----------------------------------------------------------------------
def generate_key(password, salt, iterations):
    """
    Generate salt with password
    """
    assert iterations > 0
    key = password + salt
    for i in range(iterations): # pylint: disable=W0612
        key = hashlib.sha256(key).digest()

    return key

def pad_text(text, multiple):
    """
    Pad text to 16 characters
    """
    extra_bytes = len(text) % multiple
    padding_size = multiple - extra_bytes
    padding = chr(padding_size) * padding_size
    padded_text = text + padding

    return padded_text

def unpad_text(padded_text):
    """
    Unpad text to 16 characters
    """
    padding_size = padded_text[-1]
    text = padded_text[:-padding_size]

    return text

def encrypt(plaintext):
    """
    Encrypt string
    """
    password = bytes(os.getlogin(), 'utf-8')
    salt = Crypto.Random.get_random_bytes(SALT_SIZE)
    key = generate_key(password, salt, NUMBER_OF_ITERATIONS)
    cipher = AES.new(key, AES.MODE_ECB)
    padded_plaintext = bytes(pad_text(plaintext, AES_MULTIPLE), 'utf-8')
    ciphertext = cipher.encrypt(padded_plaintext)
    ciphertext_with_salt = salt + ciphertext
    encoded_string = base64.b64encode(ciphertext_with_salt)
    encrypted_string = encoded_string.decode('utf-8')

    return encrypted_string

def decrypt(encrypted_string):
    """
    Decrypt encrypted string with password
    """
    password = bytes(os.getlogin(), 'utf-8')
    b64encoded_string = encrypted_string.encode('utf-8')
    ciphertext = base64.b64decode(b64encoded_string)
    salt = ciphertext[0:SALT_SIZE]
    ciphertext_sans_salt = ciphertext[SALT_SIZE:]
    key = generate_key(password, salt, NUMBER_OF_ITERATIONS)
    cipher = AES.new(key, AES.MODE_ECB)
    padded_plaintext = cipher.decrypt(ciphertext_sans_salt)
    plaintext_bytes = unpad_text(padded_plaintext)
    plaintext = plaintext_bytes.decode('utf-8')

    return plaintext

def update_crepo(args, config_dict):
    """
    Updates crepo
    """
    if config_dict['CREPO']['CrepoPath'] != '':
        crepo_path = os.path.expanduser(config_dict['CREPO']['CrepoPath'])
    else:
        crepo_path = os.path.expanduser(DEFAULT_CREPO_PATH)
    if os.path.exists(crepo_path):
        crepo_repo = git.Repo(crepo_path)
        assert not crepo_repo.bare
        remote_refs = crepo_repo.remote().refs
        for refs in remote_refs:
            if refs.name.rsplit('/')[-1] == args['branch']:
                crepo_repo.git.checkout(args['branch'])
                origin = crepo_repo.remote(name="origin")
                origin.pull()

def init_config(config_path):
    """
    Initializes ini config file
    """
    config_file = os.path.expanduser(config_path)
    if not os.path.exists(config_file):
        if not os.path.exists(os.path.split(config_file)[0]):
            os.makedirs(os.path.split(config_file)[0])
        with open(config_file, "x", encoding="utf-8"):
            pass
    config = configparser.ConfigParser()
    config.optionxform=str
    config['DEFAULTS'] = {'CrepoPath': DEFAULT_CREPO_PATH,
                          'DefaultRepoPath': DEFAULT_REPO_PATH,
                          'DefaultGitGroup': DEFAULT_GIT_GROUP,
                          'DefaultClusterList': DEFAULT_CLUSTER_LIST,
                          'DefaultArgoPath': DEFAULT_ARGO_PATH,
                          'GitlabBaseUrl': DEFAULT_GIT_BASE_URL}
    config['CREPO'] = {'CrepoPath': ''}
    config['GIT'] = {'DefaultRepoPath': '',
                     'DefaultGitGroup': '',
                     'DefaultClusterList': '',
                     'DefaultArgoPath': '',
                     'GitlabBaseUrl': '',
                     'GitlabAccessToken': ''}
    write_config(config, config_file)

def setup_config(config_path):
    """
    Setup config settings with user input
    """
    config_file = os.path.expanduser(config_path)

    config = configparser.ConfigParser()
    config.optionxform=str
    config.read(config_file)
    setting_key = input("Which setting do you want to change? ")
    setting_value = input("What value? ")
    if setting_key == "GitlabAccessToken":
        config.set('GIT', 'GitlabAccessToken', encrypt(setting_value))
        write_config(config, config_file)
    elif config.has_option('CREPO', setting_key):
        config.set('CREPO', setting_key, setting_value)
        write_config(config, config_file)
    elif config.has_option('GIT', setting_key):
        config.set('GIT', setting_key, setting_value)
        write_config(config, config_file)
    else:
        print("Setting " + setting_key + " does not exist, try executing 'crepo config init' and try again")    # pylint: disable=C0301

def write_config(config, config_file):
    """
    Writes stuff to ini config file
    """
    with open(config_file, mode="w", encoding="utf-8") as file:
        file.write("; Do not edit this file!\n")
        file.write("\n")
        config.write(file)

def read_config(config_path):
    """
    Reads stuff from ini config file
    """
    config = configparser.ConfigParser()
    config.optionxform=str
    config.sections()

    config_file = os.path.expanduser(config_path)
    config.read(config_file)
    config_dict={}
    sections = config.sections()
    for section in sections:
        if section != "DEFAULTS":
            items = dict(config.items(section))
            for item in items:
                if items[item] == '' and item != 'GitlabAccessToken':
                    config_dict[item] = config['DEFAULTS'][item]
                else:
                    config_dict[item] = items[item]

    return config_dict
