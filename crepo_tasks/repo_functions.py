#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Script for creating a list of clusters
"""
# -----------------------------------------------------------------------
# Import Modules
# -----------------------------------------------------------------------
import os
import json
import shutil
import git

# -----------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------
GIT_BASE_URL = "gitlab.com:"
GIT_GROUP = ""

# -----------------------------------------------------------------------
# Functions
# -----------------------------------------------------------------------
def cclone(args, config_dict):
    """
    Function that checks if to loop over file
    """
    if args['name'] is not None:
        clone_function(args, config_dict)
    elif args['file'] is not None:
        if not os.path.exists(args['file']):
            print("List file " + args['file'] + " does not exist")
        else:
            with open(args['file'], mode="r", encoding="utf-8") as list_file:
                for line in list_file:
                    args['name'] = line.strip("\n")
                    clone_function(args, config_dict)

def clone_function(args, config_dict):
    """
    Function to clone repos
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    if os.path.exists(cluster_repo_path) and args['remove'] is False and args['pull'] is False:
        print("Repo " + args['name'] + " in " + args['dest'] + " already exists.")
    elif os.path.exists(cluster_repo_path) and args['pull'] is True:
        pull_function(args)
    else:
        if os.path.exists(cluster_repo_path) and args['remove'] is True:
            shutil.rmtree(cluster_repo_path)
        repo_url = "git@" + config_dict['GitlabBaseUrl'] + ':' + args['group'] + args['name'] + ".git"
        cluster_repo = git.Repo.clone_from(repo_url, cluster_repo_path, branch=args['branch'])
        assert not cluster_repo.bare

def clist(args):
    """
    Update cluster list
    """
    # Check if name has been given
    if args['name'] is not None:
        if args['append'] is False:
            write_list(args, "w")
        else:
            write_list(args, "a")

    # Check if cluster repo exists
    elif args['path'] != '':
        # Check if repo is present
        if os.path.exists(args['path']):
            # If repo is present, pull origin
            argocd_repo = git.Repo(args['path'])
            assert not argocd_repo.bare
            origin = argocd_repo.remote(name="origin")
            origin.pull()
        else:
            # If repo is not present, clone it
            repo_name = os.path.split(os.path.dirname(args['path']))[1]
            repo_url = "git@" + GIT_BASE_URL + GIT_GROUP + repo_name + ".git"
            argocd_repo = git.Repo.clone_from(repo_url, args['path'], branch="main")
            assert not argocd_repo.bare

        clusters_path = args['path'] + "cluster-definitions/"
        # Loop over cluster folders in cluster-definitions in argo repo
        for cluster_folder in os.listdir(clusters_path):
            cluster_json_path = os.path.join(clusters_path, cluster_folder, "cluster.json")
            if args['env'] is not None:
                with open(cluster_json_path, mode="r", encoding="utf-8") as cluster_json:
                    env = json.load(cluster_json)['environment']
                    cluster_json.close()
            with open(cluster_json_path, mode="r", encoding="utf-8") as cluster_json:
                if args['env'] is not None:
                    if args['env'] == env:
                        args['name'] = json.load(cluster_json)['cluster']['name']
                        if args['append'] is False:
                            write_list(args, "w")
                            args['append'] = True
                        else:
                            write_list(args, "a")
                else:
                    args['name'] = json.load(cluster_json)['cluster']['name']
                    if args['append'] is False:
                        write_list(args, "w")
                        args['append'] = True
                    else:
                        write_list(args, "a")
                cluster_json.close()

def write_list(args, edit_mode):
    """
    Function to write clustername to file
    """
    with open(args['dest'], mode=edit_mode, encoding="utf-8") as list_file:
        list_file.write(args['name'] + "\n")
        list_file.close()

def cpull(args):
    """
    Function to check if to loop over file
    """
    if args['name'] is not None:
        pull_function(args)
    elif args['file'] is not None:
        if not os.path.exists(args['file']):
            print("List file " + args['file'] + " does not exist")
        else:
            with open(args['file'], mode="r", encoding="utf-8") as list_file:
                for line in list_file:
                    args['name'] = line.strip("\n")
                    pull_function(args)

def pull_function(args):
    """
    Function to pull repos
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    if os.path.exists(cluster_repo_path):
        cluster_repo = git.Repo(cluster_repo_path)
        assert not cluster_repo.bare
        remote_refs = cluster_repo.remote().refs
        for refs in remote_refs:
            if refs.name.rsplit('/')[-1] == args['branch']:
                cluster_repo.git.checkout(args['branch'])
                origin = cluster_repo.remote(name="origin")
                origin.pull()
