#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Script for editing cluster repos
"""
# -----------------------------------------------------------------------
# Import Modules
# -----------------------------------------------------------------------
import os
import subprocess
import git
import gitlab
from crepo_tasks import config

# -----------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------
GIT_BASE_URL = "gitlab.com:"

# -----------------------------------------------------------------------
# Functions
# -----------------------------------------------------------------------
def edit_repo(args, config_dict):
    """
    Main function for editing repos
    """
    if os.path.exists(args['dest']):
        if args['name'] is not None:
            if args['clean'] is True:
                clean_dir(args)
            create_branch(args)
            os.chdir(os.path.join(args['dest'], args['name']))
            set_type(args)
            if args['dry-run'] is True:
                dry_run(args, config_dict)
            else:
                push_change(args)
        elif args['file'] is not None:
            if not os.path.exists(args['file']):
                print("List file " + args['file'] + " does not exist")
            else:
                with open(args['file'], mode="r", encoding="utf-8") as list_file:
                    for line in list_file:
                        args['name'] = line.strip("\n")
                        if args['clean'] is True:
                            clean_dir(args)
                        os.chdir(os.path.join(args['dest'], args['name']))
                        set_type(args)
                        if args['dry-run'] is True:
                            dry_run(args, config_dict)
                        else:
                            push_change(args)

def set_type(args):
    """
    Pass program to the correct executioner
    """
    if args['type'] == "bash":
        edit_bash(args)
    elif args['type'] == "python":
        edit_python(args)

def edit_bash(args):
    """
    Edit repos with bash taskfile
    """
    if os.path.exists(args['actions']):
        os.system(args['actions'])

def edit_python(args):
    """
    Edit repos with python taskfile
    """
    subprocess.run(["python3", args['actions']], check=False)

def clean_dir(args):
    """
    Clean branch before executing tasks
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    cluster_repo = git.Repo(cluster_repo_path)
    assert not cluster_repo.bare
    remote_refs = cluster_repo.remote().refs
    for refs in remote_refs:
        if refs.name.rsplit('/')[-1] == args['branch']:
            cluster_repo.git.checkout(args['branch'])
            cluster_repo.git.reset("--hard", refs)

def create_branch(args):
    """
    Create new branch to push on
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    cluster_repo = git.Repo(cluster_repo_path)
    assert not cluster_repo.bare
    remote_refs = cluster_repo.remote().refs
    local_refs = cluster_repo.heads
    if args['branch'] not in remote_refs and args['branch'] not in local_refs:
        work_branch = cluster_repo.create_head(args['branch'])
        work_branch.checkout()

def push_change(args):
    """
    Commits and pushes changes
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    cluster_repo = git.Repo(cluster_repo_path)
    assert not cluster_repo.bare
    cluster_repo.index.add('*')
    cluster_repo.index.commit("Mass edited repo")
    origin = cluster_repo.remote(name="origin")
    origin.push()
    main_branch = cluster_repo.heads['main']
    main_branch.checkout()

def dry_run(args, config_dict):
    """
    Shows edits and undoes changes
    """
    undo_edit(args, config_dict)

def undo_edit(args, config_dict):
    """
    Undoes edits in new branch
    """
    cluster_repo_path = os.path.join(args['dest'], args['name'])
    cluster_repo = git.Repo(cluster_repo_path)
    assert not cluster_repo.bare
    remote_refs = cluster_repo.remote().refs
    local_refs = cluster_repo.heads
    if args['branch'] not in remote_refs and args['branch'] not in local_refs:
        print("Nothing to undo")
    elif args['branch'] in remote_refs or args['branch'] in local_refs:
        cluster_repo.git.reset("--hard", args['branch'])
        work_branch = cluster_repo.heads['main']
        work_branch.checkout()
        clean_dir(args)
        cluster_repo.delete_head(args['branch'])
    gl = gitlab.Gitlab("https://gitlab.com",
                       private_token=config.decrypt(config_dict['GitlabAccessToken']))
