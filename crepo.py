#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Script for cloning and pulling cluster repos
"""

# -----------------------------------------------------------------------
# Import Modules
# -----------------------------------------------------------------------
import sys
import argparse
import os
import platform
from crepo_tasks import repo_functions
from crepo_tasks import repo_edit
from crepo_tasks import config

# -----------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------
RUNNING_OS = ''
UNIX_CONFIG_PATH = "~/.config/crepo/config.ini"
WINDOWS_CONFIG_PATH = ""
CONFIG_PATH = ""

# -----------------------------------------------------------------------
# Functions
# -----------------------------------------------------------------------
def create_clone_subcommand(parser_clone, config_dict):
    """
    Create clone subcommand with arguments
    """
    # Add clone sub-arguments
    clone_megroup_retention = parser_clone.add_mutually_exclusive_group()
    clone_megroup_retention.add_argument("-r", "--remove", dest="remove",
                             action="store_true", default=False,
                             help="Remove folders before cloning. Default is false.")
    clone_megroup_retention.add_argument("-p", "--pull", dest="pull",
                             action="store_true", default=False,
                             help="Pull existing repos. Default is false.")
    parser_clone.add_argument("-b", "--branch", dest="branch",
                             metavar="BRANCH", default="main",
                             help="Branch to pull")
    parser_clone.add_argument("-d", "--dest", dest="dest",
                             metavar="PATH", default=config_dict['DefaultRepoPath'],
                             help="Path of cluster repos")
    parser_clone.add_argument("-g", "--group", dest="group",
                              metavar="GROUP", default=config_dict['DefaultGitGroup'],
                              help="Gitlab group to clone project from")
    clone_megroup_source = parser_clone.add_mutually_exclusive_group()
    clone_megroup_source.add_argument("-f", "--file", dest="file",
                              metavar="FILE", default=config_dict['DefaultClusterList'],
                              help="Path to list of clusters")
    clone_megroup_source.add_argument("-n", "--name", dest="name",
                              metavar="CLUSTERNAME",
                              help="Use cluster name instead of list file")

def create_edit_subcommand(parser_edit, config_dict):
    """
    Create edit subcommand with arguments
    """
    # Add edit sub-arguments
    parser_edit.add_argument("-b", "--branch", dest="branch",
                             metavar="BRANCH", required=True,
                             help="Branch to put changes in")
    parser_edit.add_argument("-a", "--actions", dest="actions",
                             metavar="PATH", required=True,
                             help="Path of file with actions")
    parser_edit.add_argument("-t", "--type", dest="type",
                             choices=["bash", "python"], required=True,
                             help="Filetype of action file")
    parser_edit.add_argument("-c", "--clean", dest="clean",
                             action="store_true", default=False,
                             help="Clean working branch before editing. Default is False")
    parser_edit.add_argument("--dry-run", dest="dry-run",
                             action="store_true", default=False,
                             help="Does a dry run of the edit. Default is False")
    parser_edit.add_argument("-d", "--dest", dest="dest",
                             metavar="PATH", default=config_dict['DefaultRepoPath'],
                             help="Path of cluster repos")
    parser_edit.add_argument("-g", "--group", dest="group",
                             metavar="GROUP", default=config_dict['DefaultGitGroup'],
                             help="Gitlab group to pull and push project from")
    edit_megroup = parser_edit.add_mutually_exclusive_group()
    edit_megroup.add_argument("-f", "--file", dest="file",
                              metavar="FILE", default=config_dict['DefaultClusterList'],
                              help="Path to list of clusters")
    edit_megroup.add_argument("-n", "--name", dest="name",
                              metavar="CLUSTERNAME",
                              help="Use cluster name instead of list file")

def create_list_subcommand(parser_list, config_dict):
    """
    Create list subcommand with arguments
    """
    # Add list sub-arguments
    parser_list.add_argument("-a", "--append", dest="append",
                             action="store_true", default=False,
                             help="Append to cluster list. Default is false")
    parser_list.add_argument("-d", "--dest", dest="dest",
                             metavar="PATH", default=config_dict['DefaultClusterList'],
                             help="Path of cluster list")
    parser_list.add_argument("-e", "--environment", dest="env",
                             metavar="ENVIRONMENT",
                             help="Cluster environment")
    list_megroup = parser_list.add_mutually_exclusive_group()
    list_megroup.add_argument("-n", "--name", dest="name",
                              metavar="CLUSTERNAME",
                              help="Use cluster name instead of argocd repo")
    list_megroup.add_argument("-p", "--path", dest="path",
                              metavar="PATH", default=config_dict['DefaultArgoPath'],
                              help="Location of argocd git repo")

def create_pull_subcommand(parser_pull, config_dict):
    """
    Create pull subcommand with arguments
    """
    # Add pull sub-arguments
    parser_pull.add_argument("-b", "--branch", dest="branch",
                             metavar="BRANCH", default="main",
                             help="Branch to pull")
    parser_pull.add_argument("-d", "--dest", dest="dest",
                             metavar="PATH", default=config_dict['DefaultRepoPath'],
                             help="Path of cluster repos")
    pull_megroup = parser_pull.add_mutually_exclusive_group()
    pull_megroup.add_argument("-f", "--file", dest="file",
                              metavar="FILE", default=config_dict['DefaultClusterList'],
                              help="Path to list of clusters")
    pull_megroup.add_argument("-n", "--name", dest="name",
                              metavar="CLUSTERNAME",
                              help="Use cluster name instead of list file")

def create_config_subcommand(parser_config, config_dict):
    """
    Create config subcommand with arguments
    """
    config_parsers = parser_config.add_subparsers(dest="configcommand")
    parser_update = config_parsers.add_parser("update", help="Update crepo tool",
                                              description="Updates the crepo tool")
    parser_setup = config_parsers.add_parser("setup", help="Setup crepo tool",
                                             description="Configure the crepo tool")
    parser_init = config_parsers.add_parser("init", help="Init crepo tool config",
                                             description="Initializes the crepo tool config file")

    create_update_subcommand(parser_update, config_dict)

def create_update_subcommand(parser_update, config_dict):
    """
    Create update subcommand with arguments
    """
    parser_update.add_argument("-d", "--destination", dest="dest",
                               metavar="PATH", default="~/commands/python_scripts",
                               help="Destination path of crepo tool")

def parse_clone(args, config_dict):
    """
    Parses variables and call function
    """
    if args['dest'].split('/')[0] == '~':
        args['dest'] = os.path.expanduser(args['dest'])
    else:
        args['dest'] = os.path.abspath(args['dest'])
    if args['file'].split('/')[0] == '~':
        args['file'] = os.path.expanduser(args['file'])
    else:
        args['file'] = os.path.abspath(args['file'])
    if args['name'] is not None:
        args['file'] = None
    print(args['file'])
    repo_functions.cclone(args, config_dict)

def parse_edit(args, config_dict):
    """
    Parse variables and call function
    """
    if args['actions'].split('/')[0] == '~':
        args['actions'] = os.path.expanduser(args['actions'])
    else:
        args['actions'] = os.path.abspath(args['actions'])
    if args['dest'].split('/')[0] == '~':
        args['dest'] = os.path.expanduser(args['dest'])
    else:
        args['dest'] = os.path.abspath(args['dest'])
    if args['file'].split('/')[0] == '~':
        args['file'] = os.path.expanduser(args['file'])
    else:
        args['file'] = os.path.abspath(args['file'])
    if args['name'] is not None:
        args['file'] = None
    if config_dict['GitlabAccessToken'] != '':
        repo_edit.edit_repo(args, config_dict)
    else:
        print("No authentication token for gitlab. Cannot make changes as a result.")

def parse_list(args):
    """
    Parse variables and call function
    """
    if args['dest'].split('/')[0] == '~':
        args['dest'] = os.path.expanduser(args['dest'])
    else:
        args['dest'] = os.path.abspath(args['dest'])
    if args['path'].split('/')[0] == '~':
        args['path'] = os.path.expanduser(args['path'])
    else:
        args['path'] = os.path.abspath(args['path'])
    if args['name'] is not None:
        args['path'] = None
    repo_functions.clist(args)

def parse_pull(args):
    """
    Parse variables and call function
    """
    if args['dest'].split('/')[0] == '~':
        args['dest'] = os.path.expanduser(args['dest'])
    else:
        args['dest'] = os.path.abspath(args['dest'])
    if args['file'].split('/')[0] == '~':
        args['file'] = os.path.expanduser(args['file'])
    else:
        args['file'] = os.path.abspath(args['file'])
    if args['name'] is not None:
        args['file'] = None
    repo_functions.cpull(args)

def parse_config(args, config_dict):
    """
    Parse variables and call function
    """
    if args['configcommand'] == "update":
        config.update_crepo(args, config_dict)
    elif args['configcommand'] == "setup":
        config.setup_config(CONFIG_PATH)
    elif args['configcommand'] == "init":
        config.init_config(CONFIG_PATH)

def run():
    """
    Main function to run the script
    """
    match RUNNING_OS:
        case "Linux":
            CONFIG_PATH = UNIX_CONFIG_PATH
        case "Darwin":
            CONFIG_PATH = UNIX_CONFIG_PATH
        case "Windows":
            CONFIG_PATH = os.getenv('LOCALAPPDATA') + "\crepo\config.ini"
        case _:
            print("OS %s not supported, try on supported device.", RUNNING_OS)

    if os.path.exists(os.path.expanduser(CONFIG_PATH)):
        config_dict = config.read_config(CONFIG_PATH)
    else:
        config.init_config(CONFIG_PATH)
        config_dict = config.read_config(CONFIG_PATH)

    parser = argparse.ArgumentParser(prog="crepo",
                                     description="Set-ups cluster repo(s) and edit(s) them")

    # Add Main arguments
    subparsers = parser.add_subparsers(dest="subcommand")
    parser_clone = subparsers.add_parser("clone", help="Clone help",
                                         description="Clones the cluster repo(s)")
    parser_edit = subparsers.add_parser("edit", help="Edit help",
                                        description="Edits file(s) in repo(s)")
    parser_list = subparsers.add_parser("list", help="Update cluster list",
                                        description="Creates a list of clusters")
    parser_pull = subparsers.add_parser("pull", help="Pull help",
                                        description="Pulls cluster repo(s)")
    parser_config = subparsers.add_parser("config", help="Configure crepo tool",
                                          description="Configures the crepo tool")

    # Add sub-arguments
    create_clone_subcommand(parser_clone, config_dict)
    create_edit_subcommand(parser_edit, config_dict)
    create_list_subcommand(parser_list, config_dict)
    create_pull_subcommand(parser_pull, config_dict)
    create_config_subcommand(parser_config, config_dict)

    args = vars(parser.parse_args())

    # Configure output
    if args['subcommand'] == "clone":
        parse_clone(args, config_dict)
    elif args['subcommand'] == "edit":
        parse_edit(args, config_dict)
    elif args['subcommand'] == "list":
        parse_list(args)
    elif args['subcommand'] == "pull":
        parse_pull(args)
    elif args['subcommand'] == "config":
        parse_config(args, config_dict)

# -----------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------
if __name__ == '__main__':
    # Define OS
    RUNNING_OS = platform.system()
    sys.exit(run())
