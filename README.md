# CREPO
This is a python script to edit multiple (cluster)repos at once.

![Crepo Logo](images/crepo.png)

## What is crepo?
To summarize, crepo is a tool that can clone, pull and edit multiple repos at once. The need for this script started with having to change multiple repos in the same way. Instead of editing multiple repos repetitevely there had to be a way to do it faster. For that crepo was created. The name crepo also stems from that use-case. As initialy this tool was originally planned to only be of use for cluster repos, thus the name c(luster)repo, crepo.

## Requirements
At the moment crepo is only designed for a RHEL based linux system, CentOS and Fedora should work just fine.
- Python 3.11

## Setup
To install crepo a simple bashscript can be used:
```bash
#!/bin/bash
git clone https://gitlab.com/WimBron/crepo.git
cd crepo
python3 crepo.py config init
python3 crepo.py update
```

User settings can be configured with the config subcommand.

## What's comming?
- Support for multiple OS:
    - RHEL-based systems
    - Ubuntu based systems
    - Macos
    - Windows
- Cloning multiple repos
- Pulling/updating multiple repos
- Editing multiple repos at the same time by executing a bash/python script.

## Made By
Head pancake:
 - @WimBron

## Special thanks to
Lennart for the inspiration of this tool with his small bash script that basically did the same, but with less functionality.